﻿using System.Data;
using System.Data.OleDb;

namespace TestOledbWithAccess {
    public class AccessDB {

        private string connectionString;

        private OleDbConnection _dbConnection;

        public AccessDB() {

        }
       
        public void OpenDbConnection() {
            _dbConnection = new OleDbConnection { ConnectionString = GetConnectionString() };
        }

        private string GetConnectionString() {
            return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Cedu\\Documents\\Database1.accdb";
        }

        public void CloseDbConnection() {
            _dbConnection.Close();
        }

        public void TestSelect() {
            //https://www.microsoft.com/en-us/download/confirmation.aspx?id=13255
            //64 bit dann funktionierts
            DataSet myDataSet = new DataSet();
            var myAdapptor = new OleDbDataAdapter();
            OleDbCommand command = new OleDbCommand("SELECT * FROM TestTab", _dbConnection);
            myAdapptor.SelectCommand = command;
            myAdapptor.Fill(myDataSet, "TestTab");
            var test = myDataSet;
        }
    }
}
