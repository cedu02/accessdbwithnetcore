﻿using Xunit;
using TestOledbWithAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace TestOledbWithAccess.Tests {
    public class AccessDBTests {
        [Fact()]
        public void OpenDbConnectionTest() {

            AccessDB db = new AccessDB();
            db.OpenDbConnection();
            db.TestSelect();
            Assert.True(false, "This test needs an implementation");
        }
    }
}